package br.edu.up.telas;

import java.util.Scanner;
import br.edu.up.modelo.*;

public class Menu {
    public void mostrar() {
        Scanner scanner = new Scanner(System.in);
        int escolha;

        do {
            System.out.println("Escolha uma opção:");
            System.out.println("1. Opção 1");
            System.out.println("2. Opção 2");
            System.out.println("3. Opção 3");
            System.out.println("4. Opção 4");
            System.out.println("5. Opção 5");
            System.out.println("6. Opção 6");
            System.out.println("7. Opção 7");
            System.out.println("8. Opção 8");
            System.out.println("9. Opção 9");
            System.out.println("10. Opção 10");
            System.out.println("11. Opção 11");
            System.out.println("12. Opção 12");
            System.out.println("13. Opção 13");
            System.out.println("14. Opção 14");
            System.out.println("15. Opção 15");
            System.out.println("16. Opção 16");
            System.out.println("17. Opção 17");
            System.out.println("18. Opção 18");
            System.out.println("19. Opção 19");
            System.out.println("20. Opção 20");
            System.out.println("21. Opção 21");
            System.out.println("22. Opção 22");
            System.out.println("23. Opção 23");
            System.out.println("24. Opção 24");
            System.out.println("25. Opção 25");
            System.out.println("26. Opção 26");
            System.out.println("0. Sair");
            System.out.print("Digite o número da opção desejada: ");
            escolha = scanner.nextInt();

            switch (escolha) {
                case 1:
                    System.out.println("Você escolheu a opção 1.");
                    Ex01.executar();
                    break;
                case 2:
                    System.out.println("Você escolheu a opção 2.");
                    Ex02.executar();
                    break;
                case 3:
                    System.out.println("Você escolheu a opção 3.");
                    Ex03.executar();
                    break;
                case 4:
                    System.out.println("Você escolheu a opção 4.");
                    Ex04.executar();
                    break;
                case 5:
                    System.out.println("Você escolheu a opção 5.");
                    Ex05.executar();
                    break;
                case 6:
                    System.out.println("Você escolheu a opção 6.");
                    Ex06.executar();
                    break;
                case 7:
                    System.out.println("Você escolheu a opção 7.");
                    Ex07.executar();
                    break;
                case 8:
                    System.out.println("Você escolheu a opção 8.");
                    Ex08.executar();
                    break;
                case 9:
                    System.out.println("Você escolheu a opção 9.");
                    Ex09.executar();
                    break;
                case 10:
                    System.out.println("Você escolheu a opção 10.");
                    Exercicio10.executar();
                    break;
                case 11:
                    System.out.println("Você escolheu a opção 11.");
                    Ex11.executar();
                    break;
                case 12:
                    System.out.println("Você escolheu a opção 12.");
                    Exercicio12.executar();
                    break;
                case 13:
                    System.out.println("Você escolheu a opção 13.");
                    Ex13.executar();
                    break;
                case 14:
                    System.out.println("Você escolheu a opção 14.");
                    Exercicio14.executar();
                    break;
                case 15:
                    System.out.println("Você escolheu a opção 15.");
                    Ex15.executar();
                    break;
                case 16:
                    System.out.println("Você escolheu a opção 16.");
                    Exercicio16.executar();
                    break;
                case 17:
                    System.out.println("Você escolheu a opção 17.");
                    Ex17.executar();
                    break;
                case 18:
                    System.out.println("Você escolheu a opção 18.");
                    Exercicio18.executar();
                    break;
                case 19:
                    System.out.println("Você escolheu a opção 19.");
                    Ex19.executar();
                    break;
                case 20:
                    System.out.println("Você escolheu a opção 20.");
                    Exercicio20.executar();
                    break;
                case 21:
                    System.out.println("Você escolheu a opção 21.");
                    Ex21.executar();
                    break;
                case 22:
                    System.out.println("Você escolheu a opção 22.");
                    Exercicio22.executar();
                    break;
                case 23:
                    System.out.println("Você escolheu a opção 23.");
                    Ex23.executar();
                    break;
                case 24:
                    System.out.println("Você escolheu a opção 24.");
                    Exercicio24.executar();
                    break;
                case 25:
                    System.out.println("Você escolheu a opção 25.");
                    Ex25.executar();
                    break;
                case 26:
                    System.out.println("Você escolheu a opção 26.");
                    Exercicio26.executar();
                    break;
                case 0:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida. Por favor, escolha uma opção válida.");
            }
        } while (escolha != 0);

        scanner.close();
    }
}
