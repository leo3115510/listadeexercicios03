package br.edu.up.controle;

public class Estudante {
    
    private double notaLaboratorio;
    private double notaSemestral;
    private double notaExameFinal;

    
    public double getNotaLaboratorio() {
        return notaLaboratorio;
    }


    public void setNotaLaboratorio(double notaLaboratorio) {
        this.notaLaboratorio = notaLaboratorio;
    }


    public double getNotaSemestral() {
        return notaSemestral;
    }


    public void setNotaSemestral(double notaSemestral) {
        this.notaSemestral = notaSemestral;
    }


    public double getNotaExameFinal() {
        return notaExameFinal;
    }


    public void setNotaExameFinal(double notaExameFinal) {
        this.notaExameFinal = notaExameFinal;
    }


    public Estudante() {
    }
}
