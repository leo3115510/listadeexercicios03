package br.edu.up.controle;

public class Pessoa {
    
    private String nome;
    private int idade;
    private int grupoRisco;
  
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getGrupoRisco() {
        return grupoRisco;
    }

    public void setGrupoRisco(int grupoRisco) {
        this.grupoRisco = grupoRisco;
    }

    public Pessoa() {
    }



    
    
}

