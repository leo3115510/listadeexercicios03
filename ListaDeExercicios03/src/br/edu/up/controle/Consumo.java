package br.edu.up.controle;
public class Consumo {
    private double distanciaTotal;
    private double combustivelGasto;

    public Consumo(double distanciaTotal, double combustivelGasto) {
        this.distanciaTotal = distanciaTotal;
        this.combustivelGasto = combustivelGasto;
    }

    public double calcularConsumoMedio() {
        return distanciaTotal / combustivelGasto;
    }
}
