package br.edu.up.controle;
public class Prestacao {
    private double valorCompra;

    public Prestacao(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public double calcularPrestacoes() {
        return valorCompra / 5.0;
    }
}