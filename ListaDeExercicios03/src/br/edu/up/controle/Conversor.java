package br.edu.up.controle;
public class Conversor {
    private double cotacaoDolar;
    private double quantidadeDolar;

    public Conversor(double cotacaoDolar, double quantidadeDolar) {
        this.cotacaoDolar = cotacaoDolar;
        this.quantidadeDolar = quantidadeDolar;
    }

    public double converterParaReal() {
        return cotacaoDolar * quantidadeDolar;
    }
}