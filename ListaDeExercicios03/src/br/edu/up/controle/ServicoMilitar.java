package br.edu.up.controle;
import java.util.Scanner;

public class ServicoMilitar {
    private String[] nomes;
    private char[] sexos;
    private int[] idades;
    private boolean[] saudes;
    private int totalAptas;
    private int totalInaptas;

    public ServicoMilitar(int quantidade) {
        this.nomes = new String[quantidade];
        this.sexos = new char[quantidade];
        this.idades = new int[quantidade];
        this.saudes = new boolean[quantidade];
        this.totalAptas = 0;
        this.totalInaptas = 0;
    }

    public void lerPessoas() {
        Scanner scanner = new Scanner(System.in);
        try {
            for (int i = 0; i < nomes.length; i++) {
                System.out.print("Digite o nome da pessoa " + (i + 1) + ": ");
                String nome = scanner.nextLine();

                System.out.print("Digite o sexo da pessoa " + (i + 1) + " (M/F): ");
                char sexo = scanner.next().charAt(0);
                sexo = Character.toUpperCase(sexo);

                System.out.print("Digite a idade da pessoa " + (i + 1) + ": ");
                int idade = scanner.nextInt();

                System.out.print("A pessoa " + (i + 1) + " está apta para o serviço militar? (S/N): ");
                char saudeInput = scanner.next().charAt(0);
                boolean saude = Character.toUpperCase(saudeInput) == 'S';

                scanner.nextLine(); // Consumir a nova linha pendente

                nomes[i] = nome;
                sexos[i] = sexo;
                idades[i] = idade;
                saudes[i] = saude;

                if (saude && idade >= 18) {
                    totalAptas++;
                } else {
                    totalInaptas++;
                }
            }
        } finally {
            scanner.close();
        }
    }

    public void informarAptidao() {
        for (int i = 0; i < nomes.length; i++) {
            String apta = (saudes[i] && idades[i] >= 18) ? "apta" : "não apta";
            System.out.println("Nome: " + nomes[i] + ", Sexo: " + sexos[i] + ", Idade: " + idades[i] + ", Saúde: " + apta);
        }
        System.out.println("---------------------");
        System.out.println("Total de aptas: " + totalAptas);
        System.out.println("Total de não aptas: " + totalInaptas);
    }
}
