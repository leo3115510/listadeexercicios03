package br.edu.up.controle;

import java.util.Scanner;

public class Numeros {
    private int[] numeros;

    public Numeros(int quantidade) {
        this.numeros = new int[quantidade];
    }

    public void lerNumeros() {
        Scanner scanner = new Scanner(System.in);
        try {
            for (int i = 0; i < numeros.length; i++) {
                System.out.print("Digite o " + (i + 1) + "º número: ");
                numeros[i] = scanner.nextInt();
            }
        } finally {
            scanner.close();
        }
    }

    public int contarNumerosNoIntervalo(int min, int max) {
        int quantidadeNoIntervalo = 0;
        for (int numero : numeros) {
            if (numero >= min && numero <= max) {
                quantidadeNoIntervalo++;
            }
        }
        return quantidadeNoIntervalo;
    }
}
