package br.edu.up.controle;

public class ContaDeLuz {
    private int cliente;
    private double consumoKWh;

    
    public ContaDeLuz() {
    }

    
    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    
    public double getConsumoKWh() {
        return consumoKWh;
    }

    public void setConsumoKWh(double consumoKWh) {
        this.consumoKWh = consumoKWh;
    }
}


