package br.edu.up.controle;
import java.util.Scanner;

public class Pessoas {
    private String[] nomes;
    private char[] sexos;
    private int totalHomens;
    private int totalMulheres;

    public Pessoas(int quantidade) {
        this.nomes = new String[quantidade];
        this.sexos = new char[quantidade];
        this.totalHomens = 0;
        this.totalMulheres = 0;
    }

    public void lerPessoas() {
        Scanner scanner = new Scanner(System.in);
        try {
            for (int i = 0; i < nomes.length; i++) {
                System.out.print("Digite o nome da pessoa " + (i + 1) + ": ");
                String nome = scanner.nextLine();
                System.out.print("Digite o sexo da pessoa " + (i + 1) + " (M/F): ");
                char sexo = scanner.next().charAt(0);
                scanner.nextLine(); 
                sexo = Character.toUpperCase(sexo);
                if (sexo == 'M') {
                    totalHomens++;
                } else if (sexo == 'F') {
                    totalMulheres++;
                }
                nomes[i] = nome;
                sexos[i] = sexo;
            }
        } finally {
            scanner.close();
        }
    }

    public void informarPessoas() {
        for (int i = 0; i < nomes.length; i++) {
            String genero = (sexos[i] == 'M') ? "homem" : "mulher";
            System.out.println("Nome: " + nomes[i] + ", Gênero: " + genero);
        }
        System.out.println("---------------------");
        System.out.println("Total de homens: " + totalHomens);
        System.out.println("Total de mulheres: " + totalMulheres);
    }
}
