package br.edu.up.controle;
public class Salario {
    private String nome;
    private double salarioFixo;
    private double vendasEfetuadas;

    public Salario(String nome, double salarioFixo, double vendasEfetuadas) {
        this.nome = nome;
        this.salarioFixo = salarioFixo;
        this.vendasEfetuadas = vendasEfetuadas;
    }

    public double calcular() {
        double comissao = vendasEfetuadas * 0.15;
        return salarioFixo + comissao;
    }

    public String getNome() {
        return nome;
    }

    public double getSalarioFixo() {
        return salarioFixo;
    }
}