package br.edu.up.controle;

import java.util.Scanner;

public class Funcionario {
    private Scanner scanner;
    private double totalAumentoFolha;

    // Construtor
    public Funcionario() {
        this.scanner = new Scanner(System.in);
        this.totalAumentoFolha = 0;
    }

    // Método para processar as informações do funcionário
    public void processarFuncionario() {
        try {
            System.out.print("Digite o nome do funcionário (ou 'fim' para encerrar): ");
            String nome = scanner.nextLine();
            if (nome.equals("fim")) {
                return;
            }
            System.out.print("Digite o salário do funcionário: ");
            double salario = scanner.nextDouble();

            double reajuste = salario * 0.1;
            double novoSalario = salario + reajuste;

            System.out.println("Nome do funcionário: " + nome);
            System.out.println("Reajuste: " + reajuste);
            System.out.println("Novo salário: " + novoSalario);
            System.out.println("---------------------");

            totalAumentoFolha += reajuste;
        } catch (Exception e) {
            System.out.println("Ocorreu um erro ao processar o funcionário: " + e.getMessage());
        }
    }

    // Método para obter o nome do funcionário
    public String getNome() {
        return scanner.nextLine();
    }

    // Método para informar o total de aumento na folha de pagamento
    public void informarTotalAumentoFolha() {
        System.out.println("Total de aumento na folha de pagamento: " + totalAumentoFolha);
    }

    // Método para fechar o scanner
    public void fecharScanner() {
        scanner.close();
    }
}
