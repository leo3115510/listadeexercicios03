package br.edu.up.controle;
import java.util.Scanner;

public class Concessionaria {
    private double totalDesconto;
    private double totalPago;

    public Concessionaria() {
        this.totalDesconto = 0;
        this.totalPago = 0;
    }

    public void calcularDesconto(double valor, String combustivel) {
        double desconto = 0;
        switch (combustivel) {
            case "alcool":
                desconto = valor * 0.25;
                break;
            case "gasolina":
                desconto = valor * 0.21;
                break;
            case "diesel":
                desconto = valor * 0.14;
                break;
            default:
                System.out.println("Tipo de combustível inválido!");
                return;
        }
        totalDesconto += desconto;
        double valorPago = valor - desconto;
        totalPago += valorPago;
        System.out.println("Desconto para o veículo de combustível " + combustivel + ": " + desconto);
        System.out.println("Valor a ser pago pelo cliente: " + valorPago);
        System.out.println("---------------------");
    }

    public void informarTotais() {
        System.out.println("Total de desconto: " + totalDesconto);
        System.out.println("Total pago pelos clientes: " + totalPago);
    }

    public void executarVendas() {
        Scanner scanner = new Scanner(System.in);
        try {
            while (true) {
                System.out.print("Digite o valor do veículo (ou 0 para encerrar): ");
                double valor = scanner.nextDouble();
                if (valor == 0) {
                    break;
                }
                System.out.print("Digite o tipo de combustível (álcool, gasolina ou diesel): ");
                String combustivel = scanner.next();
                calcularDesconto(valor, combustivel);
            }
        } finally {
            scanner.close();
        }
        informarTotais();
    }

    public static void main(String[] args) {
        Concessionaria concessionaria = new Concessionaria();
        concessionaria.executarVendas();
    }
}
