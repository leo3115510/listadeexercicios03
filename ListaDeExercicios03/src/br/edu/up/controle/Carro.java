package br.edu.up.controle;
public class Carro {
    private double custoFabrica;

    public Carro(double custoFabrica) {
        this.custoFabrica = custoFabrica;
    }

    public double calcularConsumidor() {
        double impostos = 0.45 * custoFabrica;
        double custoAposImpostos = custoFabrica + impostos;
        double percentagemDistribuidor = 0.28 * custoAposImpostos;
        return custoFabrica + impostos + percentagemDistribuidor;
    }
}