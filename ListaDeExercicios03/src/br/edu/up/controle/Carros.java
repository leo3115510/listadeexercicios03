package br.edu.up.controle;

public class Carros {

    private int anoDoCarro;
    private double valorDoCarro;

    public int getAnoDoCarro() {
        return anoDoCarro;
    }

    public void setAnoDoCarro(int anoDoCarro) {
        this.anoDoCarro = anoDoCarro; 
    }

    public double getValorDoCarro() {
        return valorDoCarro;
    }

    public void setValorDoCarro(double valorDoCarro) {
        this.valorDoCarro = valorDoCarro; 
    }
}

