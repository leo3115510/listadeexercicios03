package br.edu.up.controle;
public class Venda {
    private double precoCusto;
    private double percentualAcrescimo;

    public Venda(double precoCusto, double percentualAcrescimo) {
        this.precoCusto = precoCusto;
        this.percentualAcrescimo = percentualAcrescimo;
    }

    public double calcularValorVenda() {
        return precoCusto * (1 + (percentualAcrescimo / 100.0));
    }
}