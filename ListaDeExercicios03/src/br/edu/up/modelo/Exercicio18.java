package br.edu.up.modelo;
import br.edu.up.controle.Funcionarios;

public class Exercicio18 {
public static void executar(){
    Prompt.imprimir("Informe os dados do funcionário:");

        String nome = Prompt.lerLinha("Nome: ");
        int idade = Prompt.lerInteiro("Idade: ");
        char sexo = Character.toUpperCase(Prompt.lerCaractere("Sexo (M/F): "));
        double salarioFixo = Prompt.lerDecimal("Salário fixo: ");

        Funcionarios funcionario = new Funcionarios();
        funcionario.setNome(nome);
        funcionario.setIdade(idade);
        funcionario.setSexo(sexo);
        funcionario.setSalarioFixo(salarioFixo);

        double abono;
        if (sexo == 'M') {
            abono = (idade >= 30) ? 100.0 : 50.0;
        } else {
            abono = (idade >= 30) ? 200.0 : 80.0;
        }

        double salarioLiquido = salarioFixo + abono;

        

        Prompt.separador();
        Prompt.imprimir("Nome: " + funcionario.getNome());
        Prompt.imprimir("Salário líquido (com abono): R$" + salarioLiquido);
        Prompt.separador();
    }    }

