package br.edu.up.modelo;
import br.edu.up.controle.Venda;

public class Ex06 {
    public static void executar() {
        double precoCusto, percentualAcrescimo;

        precoCusto = Prompt.lerDecimal("Digite o preço de custo do produto:");
        percentualAcrescimo = Prompt.lerDecimal("Digite o percentual de acréscimo (em %):");

        Venda calculadora = new Venda(precoCusto, percentualAcrescimo);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Preço de custo do produto: R$ " + precoCusto);
        Prompt.imprimir("Percentual de acréscimo: " + percentualAcrescimo + "%");
        Prompt.imprimir("Valor de venda do produto: R$ " + calculadora.calcularValorVenda());
        Prompt.separador();
    }
}