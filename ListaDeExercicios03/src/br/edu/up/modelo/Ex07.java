package br.edu.up.modelo;
import br.edu.up.controle.Carro;
public class Ex07 {
    public static void executar() {
        double custoFabrica;

        custoFabrica = Prompt.lerDecimal("Digite o custo de fábrica do carro:");

Carro calculadora = new Carro(custoFabrica);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Custo de fábrica do carro: R$ " + custoFabrica);
        Prompt.imprimir("Custo ao consumidor do carro: R$ " + calculadora.calcularConsumidor());
        Prompt.separador();
    }
}