package br.edu.up.modelo;
import br.edu.up.controle.Produto;


    public class Exercicio14 {

    public static void executar() {

        Prompt.imprimir("Calculando lucro, prejuízo ou empate para 40 produtos:");

        double somaPrecoCusto = 0;
        double somaPrecoVenda = 0;
        
        for (int i = 1; i <= 40; i++) {
            Prompt.imprimir("Produto " + i + ":");
            double precoCusto = Prompt.lerDecimal("Digite o preço de custo: ");
            double precoVenda = Prompt.lerDecimal("Digite o preço de venda: ");

            somaPrecoCusto += precoCusto;
            somaPrecoVenda += precoVenda;

            Produto produto = new Produto();
            produto.setPrecoCusto(precoCusto);
            produto.setPrecoVenda(precoVenda);

            String resultado;
            if (precoVenda > precoCusto) {
                resultado = "Lucro";
            } else if (precoVenda < precoCusto) {
                resultado = "Prejuízo";
            } else {
                resultado = "Empate";
            }

            Prompt.imprimir("Resultado para o Produto " + i + ": " + resultado);
        }
        double mediaPrecoCusto = somaPrecoCusto / 40;
        double mediaPrecoVenda = somaPrecoVenda / 40;

        Prompt.imprimir("Média de preço de custo: " + mediaPrecoCusto);
        Prompt.imprimir("Média de preço de venda: " + mediaPrecoVenda);
    }
}