package br.edu.up.modelo;
import br.edu.up.controle.ContaDeLuz;

public class Exercicio22 {
    
    public static void executar(){

         Prompt.imprimir("Bem-vindo ao calculador de conta de luz!");

        int cliente = Prompt.lerInteiro("Informe o tipo de cliente (1 - Residência, 2 - Comércio, 3 - Indústria): ");
        double consumoKWh = Prompt.lerDecimal("Informe o consumo em kWh: ");

        ContaDeLuz contaDeLuz = new ContaDeLuz();
        contaDeLuz.setCliente(cliente);
        contaDeLuz.setConsumoKWh(consumoKWh);

        double valorKWh;

        switch (contaDeLuz.getCliente()) {
            case 1:
                valorKWh = 0.60;
                break;
            case 2:
                valorKWh = 0.48;
                break;
            case 3:
                valorKWh = 1.29;
                break;
            default:
                Prompt.imprimir("Tipo de cliente inválido. Utilizando valor padrão de R$0,60 por kWh.");
                valorKWh = 0.60;
                break;
        }

        double valorConta = valorKWh * contaDeLuz.getConsumoKWh();

        Prompt.separador();
        Prompt.imprimir("Valor da conta de luz: R$" + valorConta);
        Prompt.separador();
    }
    }
