package br.edu.up.modelo;
import br.edu.up.controle.Conversor;

public class Ex04 {
    public static void executar() {
        double cotacaoDolar, quantidadeDolar;

        cotacaoDolar = Prompt.lerDecimal("Digite a cotação do dólar (em reais):");
        quantidadeDolar = Prompt.lerDecimal("Digite a quantidade de dólares:");

        Conversor conversor = new Conversor(cotacaoDolar, quantidadeDolar);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Valor em dólar: US$ " + quantidadeDolar);
        Prompt.imprimir("Valor em real: R$ " + conversor.converterParaReal());
        Prompt.separador();
    }
}