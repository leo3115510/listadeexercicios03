package br.edu.up.modelo;
import br.edu.up.controle.Funcionario;

public class Ex17 {

    public static void executar() {
        Funcionario funcionario = new Funcionario();

        while (true) {
            funcionario.processarFuncionario();
            String nome = funcionario.getNome();
            if (nome != null && nome.equals("fim")) {
                break;
            }
        }

        funcionario.informarTotalAumentoFolha();
    }
}
