package br.edu.up.modelo;
import java.util.Scanner;

import br.edu.up.controle.Estudantes;

public class Ex25 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o nome do estudante: ");
        String nome = scanner.nextLine();
        System.out.print("Digite o número de matrícula do estudante: ");
        int matricula = scanner.nextInt();
        scanner.nextLine(); 
        System.out.print("Digite a primeira nota do estudante: ");
        double nota1 = scanner.nextDouble();
        scanner.nextLine(); 
        System.out.print("Digite a segunda nota do estudante: ");
        double nota2 = scanner.nextDouble();
        scanner.nextLine(); 
        System.out.print("Digite a terceira nota do estudante: ");
        double nota3 = scanner.nextDouble();
        scanner.nextLine(); 

        Estudantes estudante = new Estudantes(nome, matricula, nota1, nota2, nota3);
        String classificacao = estudante.classificarEstudante();
        double notaFinal = estudante.getNotaFinal();

        System.out.println("Nome do estudante: " + estudante.getNome());
        System.out.println("Número de matrícula: " + estudante.getMatricula());
        System.out.println("Nota final: " + notaFinal);
        System.out.println("Classificação: " + classificacao);
        scanner.close();
    }
}
