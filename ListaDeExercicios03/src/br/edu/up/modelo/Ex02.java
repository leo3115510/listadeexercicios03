package br.edu.up.modelo;
import br.edu.up.controle.Consumo;

public class Ex02 {
    public static void executar() {
        double distanciaTotal, combustivelGasto;

        distanciaTotal = Prompt.lerDecimal("Digite a distância total percorrida (em km):");
        combustivelGasto = Prompt.lerDecimal("Digite o total de combustível gasto (em litros):");

        Consumo consumo = new Consumo(distanciaTotal, combustivelGasto);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Consumo médio: " + consumo.calcularConsumoMedio() + " km/l");
        Prompt.separador();
    }
}