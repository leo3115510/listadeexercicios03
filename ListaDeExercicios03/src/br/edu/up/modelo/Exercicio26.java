package br.edu.up.modelo;
import br.edu.up.controle.Pessoa;

public class Exercicio26 {

    public static void executar(){

   Prompt.imprimir("Bem-vindo à Seguradora!");

        Pessoa pretendente = new Pessoa();

        String nome = Prompt.lerLinha("Digite o nome do pretendente: ");
        int idade = Prompt.lerInteiro("Digite a idade do pretendente: ");
        int grupoRisco = Prompt.lerInteiro("Digite o grupo de risco do pretendente (1 - Baixo, 2 - Médio, 3 - Alto): ");

        pretendente.setNome(nome);
        pretendente.setIdade(idade);
        pretendente.setGrupoRisco(grupoRisco);

       
        if (idade < 17 || idade > 70) {
            Prompt.imprimir("O pretendente não se enquadra em nenhuma das categorias ofertadas.");
            return;
        }

       
        String categoria = determinarCategoria(idade, grupoRisco);

        
        Prompt.separador();
        Prompt.imprimir("Dados do pretendente:");
        Prompt.imprimir("Nome: " + pretendente.getNome());
        Prompt.imprimir("Idade: " + pretendente.getIdade());
        Prompt.imprimir("Grupo de risco: " + pretendente.getGrupoRisco());
        Prompt.imprimir("Categoria de seguro: " + categoria);
        Prompt.separador();
    }

    public static String determinarCategoria(int idade, int grupoRisco) {
        if (idade >= 17 && idade <= 20) {
            switch (grupoRisco) {
                case 1:
                    return "Categoria 1";
                case 2:
                    return "Categoria 2";
                case 3:
                    return "Categoria 3";
            }
        } else if (idade >= 21 && idade <= 24) {
            switch (grupoRisco) {
                case 2:
                    return "Categoria 2";
                case 3:
                    return "Categoria 3";
                case 4:
                    return "Categoria 4";
            }
        } else if (idade >= 25 && idade <= 34) {
            switch (grupoRisco) {
                case 3:
                    return "Categoria 3";
                case 4:
                    return "Categoria 4";
                case 5:
                    return "Categoria 5";
            }
        } else if (idade >= 35 && idade <= 64) {
            switch (grupoRisco) {
                case 4:
                    return "Categoria 4";
                case 5:
                    return "Categoria 5";
                case 6:
                    return "Categoria 6";
            }
        } else if (idade >= 65 && idade <= 70) {
            switch (grupoRisco) {
                case 7:
                    return "Categoria 7";
                case 8:
                    return "Categoria 8";
                case 9:
                    return "Categoria 9";
            }
        }
        return "Categoria não encontrada";
    }
}