package br.edu.up.modelo;
import br.edu.up.controle.Estudante;

public class Exercicio24 {

    public static void executar(){

        Prompt.imprimir("Bem-vindo à calculadora de notas!");

        Estudante estudante = new Estudante();

        double notaLaboratorio = Prompt.lerDecimal("Digite a nota do trabalho de laboratório (0 a 10): ");
        double notaSemestral = Prompt.lerDecimal("Digite a nota da avaliação semestral (0 a 10): ");
        double notaExameFinal = Prompt.lerDecimal("Digite a nota do exame final (0 a 10): ");

        estudante.setNotaLaboratorio(notaLaboratorio);
        estudante.setNotaSemestral(notaSemestral);
        estudante.setNotaExameFinal(notaExameFinal);

        double notaFinal = calcularNotaFinal(estudante);

        Prompt.separador();
        Prompt.imprimir("Nota final do estudante: " + notaFinal);
        Prompt.separador();
    }

    public static double calcularNotaFinal(Estudante estudante) {
        // Pesos das notas
        double pesoLaboratorio = 2;
        double pesoSemestral = 3;
        double pesoExameFinal = 5;

        // Cálculo da nota final
        double notaFinal = (estudante.getNotaLaboratorio() * pesoLaboratorio + estudante.getNotaSemestral() * pesoSemestral + estudante.getNotaExameFinal() * pesoExameFinal) / (pesoLaboratorio + pesoSemestral + pesoExameFinal);

        return notaFinal;
    }
    
}
