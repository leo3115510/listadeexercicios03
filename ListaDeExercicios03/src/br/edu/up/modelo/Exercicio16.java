package br.edu.up.modelo;
import br.edu.up.controle.Funcionarios;

public class Exercicio16 {

    public static void executar(){

        final double salarioMinimo = 1100.0; 

        int funcionarios = 584;
        int funcionariosReajustados = 0;

        Prompt.imprimir("Calculando reajuste salarial para " + funcionarios + " funcionários...");

        for (int i = 1; i <= funcionarios; i++) {
            double salario = Prompt.lerDecimal("Informe o salário do funcionário " + i + ": ");

            Funcionarios funcionario = new Funcionarios();
            funcionario.setSalario(salario);

            double reajuste;

            if (salario < 3 * salarioMinimo) {
                reajuste = salario * 0.5; 
            } else if (salario >= 3 * salarioMinimo && salario <= 10 * salarioMinimo) {
                reajuste = salario * 0.2; 
            } else if (salario > 10 * salarioMinimo && salario <= 20 * salarioMinimo) {
                reajuste = salario * 0.15; 
            } else {
                reajuste = salario * 0.1; 
            }

            salario += reajuste;
            funcionariosReajustados++;

            Prompt.imprimir("Novo salário do funcionário " + i + ": R$" + salario);
        }

        Prompt.imprimir("Total de funcionários que receberam reajuste: " + funcionariosReajustados);
    }


    }

