package br.edu.up.modelo;
import br.edu.up.controle.ServicoMilitar;

public class Ex13 {
    public static void executar() {
        int quantidadePessoas = Prompt.lerInteiro("Digite a quantidade de pessoas a serem registradas: ");

        ServicoMilitar servicoMilitar = new ServicoMilitar(quantidadePessoas);
        servicoMilitar.lerPessoas();
        servicoMilitar.informarAptidao();
    }
}
