package br.edu.up.modelo;
import br.edu.up.controle.Aluno;

public class Ex01 {
    public static void executar() {
        String nome;
        double nota1, nota2, nota3;

        nome = Prompt.lerLinha("Digite o nome do aluno:");
        nota1 = Prompt.lerDecimal("Digite a nota da primeira prova:");
        nota2 = Prompt.lerDecimal("Digite a nota da segunda prova:");
        nota3 = Prompt.lerDecimal("Digite a nota da terceira prova:");

        Aluno aluno = new Aluno(nome, nota1, nota2, nota3);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Nome do aluno: " + aluno.getNome());
        Prompt.imprimir("Média: " + aluno.calcularMedia());
        Prompt.separador();
    }
}