package br.edu.up.modelo;
import br.edu.up.controle.Prestacao;

public class Ex05 {
    public static void executar() {
        double valorCompra;

        valorCompra = Prompt.lerDecimal("Digite o valor da compra:");

        Prestacao calculadora = new Prestacao(valorCompra);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Valor da compra: R$ " + valorCompra);
        Prompt.imprimir("Valor de cada prestação (em 5x sem juros): R$ " + calculadora.calcularPrestacoes());
        Prompt.separador();
    }
}