package br.edu.up.modelo;
import br.edu.up.controle.Numeros;

public class Ex09 {
    public static void executar() {
        int quantidadeNumeros = 80;

        Numeros numeros = new Numeros(quantidadeNumeros);
        numeros.lerNumeros();

        int quantidadeNoIntervalo = numeros.contarNumerosNoIntervalo(10, 150);

        Prompt.imprimir("Quantidade de números no intervalo entre 10 e 150: " + quantidadeNoIntervalo);
    }
}
