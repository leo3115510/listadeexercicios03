package br.edu.up.modelo;
import br.edu.up.controle.Salario;

public class Ex03 {
    public static void executar() {
        String nome;
        double salarioFixo, vendasEfetuadas;

        nome = Prompt.lerLinha("Digite o nome do vendedor:");
        salarioFixo = Prompt.lerDecimal("Digite o salário fixo do vendedor:");
        vendasEfetuadas = Prompt.lerDecimal("Digite o total de vendas efetuadas pelo vendedor:");

        Salario vendedor = new Salario(nome, salarioFixo, vendasEfetuadas);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Nome do vendedor: " + vendedor.getNome());
        Prompt.imprimir("Salário fixo: R$ " + vendedor.getSalarioFixo());
        Prompt.imprimir("Salário final: R$ " + vendedor.calcular());
        Prompt.separador();
    }
}