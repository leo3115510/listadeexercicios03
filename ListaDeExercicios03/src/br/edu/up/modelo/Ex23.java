package br.edu.up.modelo;
import java.util.Scanner;

import br.edu.up.controle.Imc;
public class Ex23 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o nome da pessoa: ");
        String nome = scanner.nextLine();
        System.out.print("Digite o sexo da pessoa (M/F): ");
        char sexo = scanner.next().charAt(0);
        System.out.print("Digite a altura da pessoa (em metros): ");
        double altura = scanner.nextDouble();
        System.out.print("Digite a idade da pessoa: ");
        int idade = scanner.nextInt();

        Imc pessoa = new Imc(nome, sexo, altura, idade);
        double pesoIdeal = pessoa.calcularPesoIdeal();
        System.out.println("Nome: " + pessoa.getNome());
        System.out.println("Peso ideal: " + pesoIdeal);
        scanner.close();
    }
}
