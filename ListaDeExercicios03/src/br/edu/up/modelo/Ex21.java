package br.edu.up.modelo;
import java.util.Scanner;

import br.edu.up.controle.Nadador;

public class Ex21 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite a idade do nadador: ");
        int idade = scanner.nextInt();

        Nadador nadador = new Nadador(idade);
        String categoria = nadador.classificarCategoria();
        System.out.println("Categoria do nadador: " + categoria);
        scanner.close();
    }
}