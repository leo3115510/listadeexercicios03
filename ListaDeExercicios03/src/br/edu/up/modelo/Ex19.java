package br.edu.up.modelo;

import br.edu.up.controle.Triangulo;
import java.util.Scanner;
public class Ex19 {
    public static void executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o valor do primeiro lado do triângulo: ");
        int lado1 = scanner.nextInt();

        System.out.print("Digite o valor do segundo lado do triângulo: ");
        int lado2 = scanner.nextInt();

        System.out.print("Digite o valor do terceiro lado do triângulo: ");
        int lado3 = scanner.nextInt();

        Triangulo triangulo = new Triangulo(lado1, lado2, lado3);
        String tipoTriangulo = triangulo.verificarTipoTriangulo();
        System.out.println(tipoTriangulo);

        scanner.close();
    }
}