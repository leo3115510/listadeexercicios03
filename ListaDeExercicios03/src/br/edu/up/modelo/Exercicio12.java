package br.edu.up.modelo;
import br.edu.up.controle.Carros;



public class Exercicio12 {

    public static void executar(){

         char continuar;
        int totalCarrosAte2000 = 0;
        int totalGeral = 0;

        do {
            Prompt.imprimir("Informe os dados do veículo:");
            int anoDoCarro = Prompt.lerInteiro("Ano do veículo: ");
            double valorDoCarro = Prompt.lerDecimal("Valor do veículo: ");

            Carros carro = new Carros();
            carro.setAnoDoCarro(anoDoCarro);
            carro.setValorDoCarro(valorDoCarro);

            double desconto;
            if (anoDoCarro <= 2000) {
                desconto = valorDoCarro * 0.12; 
                totalCarrosAte2000++;
            } else {
                desconto = valorDoCarro * 0.07; 
            }

            double valorAPagar = valorDoCarro - desconto;

            Prompt.imprimir("Desconto: R$" + desconto);
            Prompt.imprimir("Valor a ser pago pelo cliente: R$" + valorAPagar);

            totalGeral++;

            continuar = Character.toUpperCase(Prompt.lerCaractere("Deseja continuar calculando desconto? (S/N): "));
            Prompt.lerLinha(); 
        } while (continuar == 'S');

        Prompt.imprimir("Total de carros com ano até 2000: " + totalCarrosAte2000);
        Prompt.imprimir("Total geral de carros: " + totalGeral);
    }
    }

    
    

