package br.edu.up.modelo;
import br.edu.up.controle.Pessoas;

public class Ex11 {
    public static void executar() {
        int quantidadePessoas = 56;

        Pessoas pessoas = new Pessoas(quantidadePessoas);
        pessoas.lerPessoas();
        pessoas.informarPessoas();
    }
}
