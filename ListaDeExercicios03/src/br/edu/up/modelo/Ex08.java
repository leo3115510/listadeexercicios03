package br.edu.up.modelo;
import br.edu.up.controle.MencaoAluno;
public class Ex08 {
    public static void executar() {
        String nome;
        double nota1, nota2, nota3;

        nome = Prompt.lerLinha("Digite o nome do aluno:");
        nota1 = Prompt.lerDecimal("Digite a nota da primeira prova:");
        nota2 = Prompt.lerDecimal("Digite a nota da segunda prova:");
        nota3 = Prompt.lerDecimal("Digite a nota da terceira prova:");

        MencaoAluno calculadora = new MencaoAluno(nome, nota1, nota2, nota3);

        Prompt.linhaEmBranco();
        Prompt.separador();
        Prompt.imprimir("Nome do aluno: " + calculadora.getNome());
        Prompt.imprimir("Média: " + calculadora.calcularMedia());
        Prompt.imprimir("Menção: " + calculadora.calcularMencao());
        Prompt.separador();
    }
}