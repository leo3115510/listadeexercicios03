package br.edu.up.modelo;
import br.edu.up.controle.Professor;

public class Exercicio20 {
    
    public static void executar(){

          Prompt.imprimir("Informe os dados do professor:");

        int nivel = Prompt.lerInteiro("Nível do professor (1, 2 ou 3): ");
        int horasTrabalhadas = Prompt.lerInteiro("Quantidade de horas trabalhadas: ");

        Professor professor = new Professor();
        professor.setNivel(nivel);
        professor.setHorasTrabalhadas(horasTrabalhadas);

        double valorHoraAula;

        switch (professor.getNivel()) {
            case 1:
                valorHoraAula = 12.0;
                break;
            case 2:
                valorHoraAula = 17.0;
                break;
            case 3:
                valorHoraAula = 25.0;
                break;
            default:
                Prompt.imprimir("Nível inválido. Utilizando valor padrão de R$12,00 por hora/aula.");
                valorHoraAula = 12.0;
                break;
        }

        double salario = valorHoraAula * professor.getHorasTrabalhadas();

        Prompt.separador();
        Prompt.imprimir("Salário do professor: R$" + salario);
        Prompt.separador();
    }
    }

